class Track {
    constructor(mmsi,tracktype,selected,serverutc) {
      this.mmsi = mmsi;
      this.tracktype = tracktype;
      this.hit = false;
      this.marker = null;
      this.historyLine = null;
      this.projectionLine = null;
      this.projectionPoint = null;
      this.projectionPeriod = 3600; // 1h
      this.projectionText = this.projectionPeriod/3600 + "h";
      this.circlemarker = null;
      this.circlesRadiusmarkersLayerGroup = L.layerGroup(); 
      this.circlesRadiusmarkers = [];
      this.circleDetectionmarker = null;
      this.selected = selected;
      this.adbName = null;
      this.adbDimension = null;
      this.adbType = null;
      this.adbClassification = null;
      this.sliderSpeed = null;
      this.sliderCourse = null;
      this.serverutc = serverutc;
    }
    get innerHTMLDetails() {
        var html = "";
        if (this.adbName != null) {
            html += "<strong>Classification Info from TrackActorDB:</strong><br>";
            html += "<strong>Name:</strong> " +  this.adbName + "<br>";
            html += "<strong>Type:</strong> " +  this.adbType + "<br>";
            html += "<strong>Dimension:</strong> " +  this.adbDimension + "<br>";
            html += "<strong>Classification:</strong> " +  this.adbClassification + "<br><hr>";
        }
        return html;
    }
    get formattedReceivedDate(){
        if(Number.isInteger(this.serverutc)){
            var options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' , hour12: false};
            options.timeZone = 'UTC';
            options.timeZoneName = 'short';
    
            const enUSFormatter = new Intl.DateTimeFormat('en-US', options);
            return enUSFormatter.format(new Date(this.serverutc));
          }
          else {
            return "Date Not provided.";  
          }
    }
    static async loadDetections(track) {
        let url = `${window.origin}/${track.mmsi}/detections`;
        try {
            let res = await fetch(url);
            let detections = await res.json();
            track.detection = [];
            for (let i = 0; i < detections.length; i++) {
                track.detection[i] = detections[i];
            }
        } catch (error) {
            console.log(error);
        }
    }
    set setADBName(adbName) {
        this.adbName = adbName;
    }
    set setADBDimension(adbDimension) {
        this.adbDimension = adbDimension;
    }    
    set setADBType(adbType) {
        this.adbType = adbType;
    }
    set setADBClassification(adbClassification) {
        this.adbClassification = adbClassification;
    }   
    showType() {
      return 'Track id(mmsi) = ' + this.mmsi + ' is type ' + this.tracktype;
    }
  }

class OwnShip extends Track {
    constructor(builder) {
      super(builder.mmsi,"ownship", false, builder.serverutc);  
      this.fullName = builder.fullName;
      this.latitude = builder.latitude;
      this.longitude = builder.longitude;
      this.courseOverGround = builder.courseOverGround;
      this.speedOverGround = builder.speedOverGround;
      this.fuelInLiters = builder.fuelInLiters;
      this.dBNoise = builder.dBNoise;
      this.detectionRange = builder.detectionRange;
      this.detectionAzimuthRange = builder.detectionAzimuthRange;
      this.hit = builder.hit;
      this.detection = [];
    }
    get icon() {
        var shipOwnIcon = L.icon({ iconUrl: 'static/ownship.png', iconAnchor: [5, 5]});
        var shipOwnIconSelected = L.icon({ iconUrl: 'static/ownship-selected.png', iconAnchor: [5, 5]});
        var shipOwnIconTargeted = L.icon({ iconUrl: 'static/ownship-targeted.png', iconAnchor: [5, 5]});
        var shipOwnIconTargetedSelected = L.icon({ iconUrl: 'static/ownship-targeted-selected.png', iconAnchor: [5, 5]});
        if(!this.hit){
            if(this.selected){
                return shipOwnIconSelected;
            }
            else{
                return shipOwnIcon;
            }
        }
        else{
            if(this.selected){
                return shipOwnIconTargetedSelected;
            }
            else{
                return shipOwnIconTargeted;
            }
        }
    }
    get innerHTMLDetails() {
        var html = super.innerHTMLDetails;
        if(this.hit){
            html += `<strong style="color: red">TARGETED SHIP : <BR> No Operational Conditions</strong><br>`;
        }
        html += "<strong>MMSI:</strong> " +  this.mmsi + "<br>";
        html += "<strong>Latitude:</strong> " +  this.latitude + "<br>";
        html += "<strong>Longitude:</strong> " +  this.longitude + "<br>";
        html += "<strong>Course Over Ground:</strong> " +  this.courseOverGround + "&deg;<br>";
        html += "<strong>Speed Over Ground:</strong> " +  this.speedOverGround + " knots<br>";
        html += "<strong>Fuel:</strong> " +  this.fuelInLiters.toFixed(2) + " liters (Max: 1000000L) <br>";
        html += `<strong style="color: orange">Self Noise (Orange Area):</strong> ` +  this.dBNoise.toFixed(1) + ` dB <br>`;
        html += `<strong style="color: blue">Detection Parameters (Blue Area):</strong><br>`;
        html += `<strong style="color: blue">Detection Azimuth Range:</strong> ` +  this.detectionAzimuthRange + ` &deg;<br>`;
        html += `<strong style="color: blue">Detection Distance Range:</strong> ` +  this.detectionRange.toFixed(0) + ` m <br>`;
        html += "<strong>Received at:</strong> " +  this.formattedReceivedDate + "<br>";
        html += "<strong>Detections:</strong><br>";
        for (let i = 0; i < this.detection.length; i++) {
            html += "<ul>";
            html += "<li> Azimuth:" + this.detection[i].azimuth.toFixed(2) + " &deg; Noise: " + this.detection[i].dBNoise.toFixed(1) + "dB <br>";
            html += "Distance:" + this.detection[i].distance.toFixed(0) + " m. <br>";
            html += "Lat/Lng ("+ this.detection[i].latitude+","+ this.detection[i].longitude+") </li>";
            html += "</ul>";
        }
        return html;
    }
    set setSoG(speed) {
        let url = `${window.origin}/${this.mmsi}/sog/${speed}`;
        try {
            fetch(url);
        } catch (error) {
            console.log(error);
        }
    }
    set setCoG(course) {
        let url = `${window.origin}/${this.mmsi}/cog/${course}`;
        try {
            fetch(url);
        } catch (error) {
            console.log(error);
        }
    }
    static get Builder() {
        class Builder {
           constructor(mmsi) {
                this.mmsi = mmsi;
           }
           withName(fullName) {
                this.fullName = fullName;
                return this;
           }
           withLatitude(latitude) {
                this.latitude = latitude;
                return this;
           }
           withLongitude(longitude) {
                this.longitude = longitude;
                return this;
           }
           withCoG(courseOverGround) {
            this.courseOverGround = courseOverGround;
            return this;
           }
           withSoG(speedOverGround) {
            this.speedOverGround = speedOverGround;
            return this;
           }
           withFuelInLiters(fuelInLiters) {
            this.fuelInLiters = fuelInLiters;
            return this;
           }
           withNoiseInDb(dBNoise) {
            this.dBNoise = dBNoise;
            return this;
           }
           withDetectionRange(detectionRange) {
            this.detectionRange = detectionRange;
            return this;
           }
           withDetectionAzimuthRange(detectionAzimuthRange) {
            this.detectionAzimuthRange = detectionAzimuthRange;
            return this;
           }
           withServerUTC(serverutc) {
            this.serverutc = serverutc;
            return this;
           }
           withHit(hit) {
            this.hit = hit;
            return this;
           }
           build() {
                return new OwnShip(this);
           }
        }
        return Builder;
     }
  }

  class AISTrack extends Track {
    constructor(builder) {
      super(builder.mmsi,"aistrack", false, builder.serverutc);
      this.messageType = builder.messageType;
      this.navigationStatus = builder.navigationStatus;  
      this.fullName = builder.fullName;
      this.latitude = builder.latitude;
      this.longitude = builder.longitude;
      this.courseOverGround = builder.courseOverGround;
      this.speedOverGround = builder.speedOverGround;
      this.altitude = builder.altitude;
      this.trueHeading = builder.trueHeading;
      this.rateOfTurn = builder.rateOfTurn;
      this.positionAccuracy = builder.positionAccuracy;
      this.specialManeuverIndicator = builder.specialManeuverIndicator;
      this.raimFlag = builder.raimFlag;
      this.communicationState = builder.communicationState;
      this.syncState = builder.syncState;
      this.slotIncrement = builder.slotIncrement;
      this.numberOfSlots = builder.numberOfSlots;
      this.keepFlag = builder.keepFlag;
      this.slotTimeout = builder.slotTimeout;
      this.numberOfReceivedStations = builder.numberOfReceivedStations;
      this.slotNumber = builder.slotNumber;
      this.slotOffset = builder.slotOffset;
      this.utcHour = builder.utcHour;
      this.utcMinute = builder.utcMinute;
      this.imo = builder.imo;
      this.callsign = builder.callsign;
      this.shipName = builder.shipName;
      this.shipType = builder.shipType;
      this.toBow = builder.toBow;
      this.toStern = builder.toStern;
      this.toStarboard = builder.toStarboard;
      this.toPort = builder.toPort;
      this.positionFixingDevice = builder.positionFixingDevice;
      this.destination = builder.destination;
      this.destinationMmsi = builder.destinationMmsi;
      this.etaMonth = builder.etaMonth;
      this.etaDay = builder.etaDay;
      this.etaHour = builder.etaHour;
      this.etaMinute = builder.etaMinute;
      this.draught = builder.draught;
      this.dataTerminalReady = builder.dataTerminalReady;
      this.sequenceNumber = builder.sequenceNumber;
      this.retransmit = builder.retransmit;
      this.transponderClass = builder.transponderClass;
      this.spare = builder.spare;
      this.designatedAreaCode = builder.designatedAreaCode;
      this.repeatIndicator = builder.repeatIndicator;
      this.functionalId = builder.functionalId;
      this.binaryData = builder.binaryData;
      this.applicationSpecificMessage = builder.applicationSpecificMessage;
      this.valid = builder.valid;
      this.second = builder.second;
    }
    get icon() {
        let filepath = 'static/';
        let anchorX, anchorY = 0;
        switch(this.messageType) {
            case 'BASE_STATION_REPORT':
                filepath += "icon-base";
                anchorX = 17;
                anchorY = 38;
                break;
            case 'STANDARD_SAR_AIRCRAFT_POSITION_REPORT':
                filepath += "icon-airplane";
                anchorX = 12;
                anchorY = 12;
                break;
            default:
                filepath += "ship-generic";
                anchorX = 5;
                anchorY = 5;
          }
          switch(this.adbClassification) {
            case 'FRIEND':
                filepath += "-friend";
                break;
            case 'HOSTILE':
                filepath += "-hostile";
                break;
            default:
          }
          if(this.selected){
            filepath += "-selected.png";
          }
          else{
            filepath += ".png";  
          }
        return L.icon({iconUrl: filepath,iconAnchor: [anchorX, anchorY]}); 
    }
    get innerHTMLDetails() {
        var html = super.innerHTMLDetails;
        html += "<strong>MMSI:</strong> " +  this.mmsi + "<br>";
        html += "<strong>Message Type:</strong> " +  this.messageType.replace(/_/g, " ") + "<br>"; 
        if (this.navigationStatus != null) {
            html += "<strong>Navigation Status:</strong> " +  this.navigationStatus + "<br>";
        }
        html += "<strong>Latitude:</strong> " +  this.latitude + "<br>";
        html += "<strong>Longitude:</strong> " +  this.longitude + "<br>";
        if (this.altitude != null) {
            html += "<strong>Altitude:</strong> " +  this.altitude + "<br>";
        }
        html += "<strong>Course Over Ground:</strong> " +  this.courseOverGround + "&deg;<br>";
        html += "<strong>Speed Over Ground:</strong> " +  this.speedOverGround + " knots<br>";
        if (this.trueHeading != null) {
            html += "<strong>True Heading:</strong> " +  this.trueHeading + "&deg;<br>";
        }
        if (this.rateOfTurn != null) {
            html += "<strong>Rate Of Turn:</strong> " +  this.rateOfTurn + "<br>";
        }
        if (this.positionAccuracy != null) {
            html += "<strong>Position Accuracy:</strong> " +  this.positionAccuracy + "<br>";
        }
        if (this.specialManeuverIndicator != null) {
            html += "<strong>Special Maneuver Indicator:</strong> " +  this.specialManeuverIndicator + "<br>";
        }
        if (this.raimFlag != null) {
            html += "<strong>Raim Flag:</strong> " +  this.raimFlag + "<br>";
        } 
        if (this.communicationState != null) {
            html += "<strong>Communication State:</strong> " +  this.communicationState + "<br>";
        }
        if (typeof this.syncState !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Sync State:</strong> " +  this.syncState + "<br>";
        }
        if (typeof this.slotIncrement !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Slot Increment:</strong> " +  this.slotIncrement + "<br>";
        }
        if (typeof this.numberOfSlots !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Number of Slots:</strong> " +  this.numberOfSlots + "<br>";
        }
        if (typeof this.keepFlag !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Keep Flag:</strong> " +  this.keepFlag + "<br>";
        }
        if (typeof this.slotTimeout !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Slot Timeout:</strong> " +  this.slotTimeout + "<br>";
        }
        if (typeof this.numberOfReceivedStations !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Number of Received Stations:</strong> " +  this.numberOfReceivedStations + "<br>";
        }
        if (typeof this.slotNumber !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Slot Number:</strong> " +  this.slotNumber + "<br>";
        }
        if (typeof this.utcHour !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>UTC Hour:</strong> " +  this.utcHour + "<br>";
        }
        if (typeof this.utcMinute !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>UTC Minute:</strong> " +  this.utcMinute + "<br>";
        }
        if (typeof this.slotOffset !== 'undefined') {
            html += "&nbsp;&nbsp;<strong>Slot Offset:</strong> " +  this.slotOffset + "<br>";
        }
        if (this.imo != null) {
            html += "<strong>IMO:</strong> " +  this.imo + "<br>";
        }
        if (this.callsign != null) {
            html += "<strong>CallSign:</strong> " +  this.callsign + "<br>";
        }
        if (this.shipName != null) {
            html += "<strong>Ship Name:</strong> " +  this.shipName + "<br>";
        }
        if (this.shipType != null) {
            html += "<strong>Ship Type:</strong> " +  this.shipType + "<br>";
        }
        if (this.toBow != 0 && typeof this.toBow !== 'undefined') {
            html += "<strong>Distance to Bow:</strong> " +  this.toBow + "<br>";
        }
        if (this.toStern != 0 && typeof this.toStern !== 'undefined') {
            html += "<strong>Distance to Stern:</strong> " +  this.toStern + "<br>";
        }
        if (this.toStarboard != 0 && typeof this.toStarboard !== 'undefined') {
            html += "<strong>Distance to Starboard:</strong> " +  this.toStarboard + "<br>";
        }
        if (this.toPort != 0 && typeof this.toPort !== 'undefined') {
            html += "<strong>Distance to Port:</strong> " +  this.toPort + "<br>";
        }
        if (this.positionFixingDevice != null) {
            html += "<strong>Position Fixing Device:</strong> " +  this.positionFixingDevice + "<br>";
        }
        if (this.destination != null) {
            html += "<strong>Destination (MMSI):</strong> " +  this.destination + " ("+ this.destinationMmsi +") <br>";
            html += "<strong>ETA Month:</strong> " +  this.etaMonth + "<br>";
            html += "<strong>ETA Day:</strong> " +  this.etaDay + "<br>";
            html += "<strong>ETA Hour:</strong> " +  this.etaHour + "<br>";
            html += "<strong>ETA Minute:</strong> " +  this.etaMinute + "<br>";
        }  
        if (typeof this.draught !== 'undefined') {
            html += "<strong>Draught:</strong> " +  this.draught + "<br>";
        }
        if (typeof this.dataTerminalReady !== 'undefined') {
            html += "<strong>Data Termina Ready:</strong> " +  this.dataTerminalReady + "<br>";
        }
        if (typeof this.sequenceNumber !== 'undefined') {
            html += "<strong>Sequence Number:</strong> " +  this.sequenceNumber + "<br>";
        }
        if (typeof this.retransmit !== 'undefined') {
            html += "<strong>Retransmit:</strong> " +  this.retransmit + "<br>";
        }
        if (typeof this.transponderClass !== 'undefined') {
            html += "<strong>Transponder Class:</strong> " +  this.transponderClass + "<br>";
        }       
        if (typeof this.spare !== 'undefined') {
            html += "<strong>Spare:</strong> " +  this.spare + "<br>";
        }
        if (typeof this.designatedAreaCode !== 'undefined') {
            html += "<strong>Designated Area Code:</strong> " +  this.designatedAreaCode + "<br>";
        } 
        if (typeof this.repeatIndicator !== 'undefined') {
            html += "<strong>Repeat Indicator:</strong> " +  this.repeatIndicator + "<br>";
        }  
        if (typeof this.functionalId !== 'undefined') {
            html += "<strong>Functional Id:</strong> " +  this.functionalId + "<br>";
        }
        if (this.binaryData != null) {
            html += "<strong>Binary Data:</strong> " +  this.binaryData + "<br>";
        }
        if (this.applicationSpecificMessage != null) {
            html += "<strong>Specific Message:</strong> " +  this.applicationSpecificMessage + "<br>";
        } 
        if (typeof this.valid !== 'undefined') {
            html += "<strong>Valid:</strong> " +  this.valid + "<br>";
        }
        if (typeof this.received !== 'undefined') {
            html += "<strong>Received:</strong> " +  this.received + "<br>";
        }
        if (typeof this.second !== 'undefined') {
            html += "<strong>Second:</strong> " +  this.second + "<br>";
        } 
        html += "<strong>Received at:</strong> " +  this.formattedReceivedDate + "<br>";
        return html;
    }
    static get Builder() {
        class Builder {
           constructor(mmsi) {
                this.mmsi = mmsi;
           }
           messageType
           withMessageType(messageType) {
            this.messageType = messageType;
            return this;
           }
           withNavigationStatus(navigationStatus) {
            this.NavigationStatus = navigationStatus;
            return this;
           }
           withName(fullName) {
                this.fullName = fullName;
                return this;
           }
           withLatitude(latitude) {
                this.latitude = latitude;
                return this;
           }
           withLongitude(longitude) {
                this.longitude = longitude;
                return this;
           }
           withAltitude(altitude) {
                this.altitude = altitude;
                return this;
           }           
           withCoG(courseOverGround) {
                this.courseOverGround = courseOverGround;
                return this;
           }
           withSoG(speedOverGround) {
                this.speedOverGround = speedOverGround;
                return this;
           }
           withTrueHeading(trueHeading) {
                this.trueHeading = trueHeading;
                return this;
           }           
           withRateOfTurn(rateOfTurn) {
                this.rateOfTurn = rateOfTurn;
                return this;
           }  
           withPositionAccuracy(positionAccuracy) {
                this.positionAccuracy = positionAccuracy;
                return this;
           }  
           withSpecialManeuverIndicator(specialManeuverIndicator) {
                this.specialManeuverIndicator = specialManeuverIndicator;
                return this;
           }  
           withRaimFlag(raimFlag) {
            this.raimFlag = raimFlag;
            return this;
           }  
           withCommunicationState(communicationState) {
            this.communicationState = communicationState;
            return this;
           }  
           withSyncState(syncState) {
            this.syncState = syncState;
            return this;
           }  
           withSlotIncrement(slotIncrement) {
            this.slotIncrement = slotIncrement;
            return this;
           }  
           withNumberOfSlots(numberOfSlots) {
            this.numberOfSlots = numberOfSlots;
            return this;
           }  
           withKeepFlag(keepFlag) {
            this.keepFlag = keepFlag;
            return this;
           }  
           withSlotTimeout(slotTimeout) {
            this.slotTimeout = slotTimeout;
            return this;
           }  
           withNumberOfReceivedStations(numberOfReceivedStations) {
            this.numberOfReceivedStations = numberOfReceivedStations;
            return this;
           }  
           withSlotNumber(slotNumber) {
            this.slotNumber = slotNumber;
            return this;
           }  
           withSlotOffset(slotOffset) {
            this.slotOffset = slotOffset;
            return this;
           }  
           withUtcHour(utcHour) {
            this.utcHour = utcHour;
            return this;
           }  
           withUtcMinute(utcMinute) {
            this.utcMinute = utcMinute;
            return this;
           }     
           withIMO(imo) {
            this.imo = imo;
            return this;
           }      
           withCallsign(callsign) {
            this.callsign = callsign;
            return this;
           } 
           withShipName(shipName) {
            this.shipName = shipName;
            return this;
           } 
           withShipType(shipType) {
            this.shipType = shipType;
            return this;
           } 
           withToBow(toBow) {
            this.toBow = toBow;
            return this;
           } 
           withToStern(toStern) {
            this.toStern = toStern;
            return this;
           } 
           withToStarboard(toStarboard) {
            this.toStarboard = toStarboard;
            return this;
           }
           withToPort(toPort) {
            this.toPort = toPort;
            return this;
           } 
           withPositionFixingDevice(positionFixingDevice) {
            this.positionFixingDevice = positionFixingDevice;
            return this;
           } 
           withDestination(destination) {
            this.destination = destination;
            return this;
           } 
           withDestinationMmsi(destinationMmsi) {
            this.destinationMmsi = destinationMmsi;
            return this;
           } 
           withETAMonth(etaMonth) {
            this.etaMonth = etaMonth;
            return this;
           }
           withETADay(etaDay) {
            this.etaDay = etaDay;
            return this;
           } 
           withETAHour(etaHour) {
            this.etaHour = etaHour;
            return this;
           }
           withETAMinute(etaMinute) {
            this.etaMinute = etaMinute;
            return this;
           } 
           withDraught(draught) {
            this.draught = draught;
            return this;
           } 
           withDataTerminalReady(dataTerminalReady) {
            this.dataTerminalReady = dataTerminalReady;
            return this;
           }
           withSequenceNumber(sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
           } 
           withRetransmit(retransmit) {
            this.retransmit = retransmit;
            return this;
           }
           withTransponderClass(transponderClass) {
            this.transponderClass = transponderClass;
            return this;
           } 
           withSpare(spare) {
            this.spare = spare;
            return this;
           }
           withDesignatedAreaCode(designatedAreaCode) {
            this.designatedAreaCode = designatedAreaCode;
            return this;
           } 
           withRepeatIndicator(repeatIndicator) {
            this.repeatIndicator = repeatIndicator;
            return this;
           }
           withFunctionalId(functionalId) {
            this.functionalId = functionalId;
            return this;
           } 
           withBinaryData(binaryData) {
            this.binaryData = binaryData;
            return this;
           }
           withApplicationSpecificMessage(applicationSpecificMessage) {
            this.applicationSpecificMessage = applicationSpecificMessage;
            return this;
           } 
           withValid(valid) {
            this.valid = valid;
            return this;
           }
           withSecond(second) {
            this.second = second;
            return this;
           } 
           withServerUTC(serverutc) {
            this.serverutc = serverutc;
            return this;
           }
           build() {
                return new AISTrack(this);
           }
        }
        return Builder;
     }
  }
  
  class Weapon extends Track {
    constructor(builder) {
      super(builder.wid,"weapon", false, builder.serverutc); 
      this.wid = builder.wid; 
      this.fullName = builder.fullName;
      this.latitude = builder.latitude;
      this.longitude = builder.longitude;
      this.courseOverGround = builder.courseOverGround;
      this.speedOverGround = builder.speedOverGround;
      this.fuelInLiters = builder.fuelInLiters;
      this.dBNoise = builder.dBNoise;
      this.detectionRange = builder.detectionRange;
      this.detectionAzimuthRange = builder.detectionAzimuthRange;
      this.distanceTravelled = builder.distanceTravelled;
      this.circleAutoGuidedDetectionMarker = null;
    }
    get icon() {
        var WeaponIcon = L.icon({
            iconUrl: 'static/weapon.png',
            iconAnchor: [18, 3]
        });
        var WeaponIconSelected = L.icon({
            iconUrl: 'static/weapon-selected.png',
            iconAnchor: [18, 3]
        });
        if(this.selected){
            return WeaponIconSelected;
        }
        else{
            return WeaponIcon;
        }
    }
    get innerHTMLDetails() {
        var html = super.innerHTMLDetails;
        html += "<strong>WID:</strong> " +  this.wid + "<br>";
        html += "<strong>Latitude:</strong> " +  this.latitude + "<br>";
        html += "<strong>Longitude:</strong> " +  this.longitude + "<br>";
        html += "<strong>Course Over Ground:</strong> " +  this.courseOverGround + "&deg;<br>";
        html += "<strong>Speed Over Ground:</strong> " +  this.speedOverGround + " knots<br>";
        html += "<strong>Distance Travelled:</strong> " +  this.distanceTravelled + "<br>";
        html += "<strong>Fuel:</strong> " +  this.fuelInLiters.toFixed(2) + " liters (Max: 1000000L) <br>";
        html += `<strong style="color: orange">Self Noise (Orange Area):</strong> ` +  this.dBNoise.toFixed(1) + ` dB <br>`;
        html += `<strong style="color: blue">Detection Parameters (Blue Area):</strong><br>`;
        html += `<strong style="color: blue">Detection Azimuth Range:</strong> ` +  this.detectionAzimuthRange + ` &deg;<br>`;
        html += `<strong style="color: blue">Detection Distance Range:</strong> ` +  this.detectionRange.toFixed(0) + ` m <br>`;
        html += `<strong style="color: red">AutoGuided Parameters (Red Area):</strong><br>`;
        html += `<strong style="color: red">AutoGuided Azimuth Range:</strong> 15&deg;<br>`;
        html += `<strong style="color: red">AutoGuided Azimuth Gain:</strong> 3<br>`;
        html += "<strong>Detections:</strong><br>";
        for (let i = 0; i < this.detection.length; i++) {
            html += "<ul>";
            html += "<li> Azimuth:" + this.detection[i].azimuth.toFixed(2) + " &deg; Noise: " + this.detection[i].dBNoise.toFixed(1) + "dB <br>";
            html += "Distance:" + this.detection[i].distance.toFixed(0) + " m. <br>";
            html += "Lat/Lng ("+ this.detection[i].latitude+","+ this.detection[i].longitude+") </li>";
            html += "</ul>";
        }
        html += "<strong>Received at:</strong> " +  this.formattedReceivedDate + "<br>";
        return html;
    }
    static get Builder() {
        class Builder {
           constructor(wid) {
                this.wid = wid;
           }
           withName(fullName) {
                this.fullName = fullName;
                return this;
           }
           withLatitude(latitude) {
                this.latitude = latitude;
                return this;
           }
           withLongitude(longitude) {
                this.longitude = longitude;
                return this;
           }
           withCoG(courseOverGround) {
            this.courseOverGround = courseOverGround;
            return this;
           }
           withSoG(speedOverGround) {
            this.speedOverGround = speedOverGround;
            return this;
           }
           withDistanceTravelled(distanceTravelled) {
            this.distanceTravelled = distanceTravelled;
            return this;
           }
           withFuelInLiters(fuelInLiters) {
            this.fuelInLiters = fuelInLiters;
            return this;
           }
           withNoiseInDb(dBNoise) {
            this.dBNoise = dBNoise;
            return this;
           }
           withDetectionRange(detectionRange) {
            this.detectionRange = detectionRange;
            return this;
           }
           withDetectionAzimuthRange(detectionAzimuthRange) {
            this.detectionAzimuthRange = detectionAzimuthRange;
            return this;
           }
           withServerUTC(serverutc) {
            this.serverutc = serverutc;
            return this;
           }
           build() {
                return new Weapon(this);
           }
        }
        return Builder;
     }
  }